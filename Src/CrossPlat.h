#pragma once
#include <ctime>


#ifdef WIN32
    #define LOCAL_TIME(tm, time) localtime_s(tm, time)
    #define PIPE_OPEN(cmd, mode) _popen(cmd, mode)
    #define PIPE_CLOSE(fp) _pclose(fp)

#else
    #define LOCAL_TIME(tm, time) localtime_r(time,tm)
    #define PIPE_OPEN(cmd, mode) popen(cmd, mode)
    #define PIPE_CLOSE(fp) pclose(fp)
#endif // WIN32