#pragma once
#include <string>
#ifdef WIN32
    #ifdef API_HTTP_EXPOT
        #define HTTP_API __declspec(dllexport)
    #else
        #define HTTP_API __declspec(dllimport)
    #endif
#else
    #define HTTP_API __attribute__ ((visibility("default")))
#endif




typedef void(*callback_request)(const std::string& strUri, const std::string& strBody, std::string& strResponse);


HTTP_API bool StartHttpServer(int nPort, callback_request cbResponse, int nSslPort = 0);


HTTP_API void StopHttpServer();


