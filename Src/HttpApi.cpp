#define API_HTTP_EXPOT
#include "HttpApi.h"
#include "HttpServer.h"


bool StartHttpServer(int nPort, callback_request cbResponse, int nSslPort/* = 0 */)
{
    return HttpServer::Instance().StartHttpServer(nPort, cbResponse, nSslPort);
}


void StopHttpServer()
{
    HttpServer::Instance().StopHttpServer();
}