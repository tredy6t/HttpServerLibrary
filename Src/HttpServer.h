#pragma once
#include <map>
#include <string>
#include <atomic>
#include "mongoose/mongoose.h"
#include <thread>
#include "HttpApi.h"


class HttpServer
{
private:
    HttpServer();
public:
    static HttpServer& Instance();
    ~HttpServer();
    bool StartHttpServer(int nPort, callback_request cbResponse = nullptr, int nSslPort = 0);
    void StopHttpServer();

    static void event_handle(struct mg_connection* pConn, int ev, void* pEventData, void* pPara);
private:
    void run();
    void stop();
    std::string do_request(const std::string& strUri, const std::string& strBody);


private:
    mg_mgr m_Mgr;   // manager of event
    std::atomic<bool> m_bExit;
    std::thread m_thRun;
    callback_request m_cbResponse;

};