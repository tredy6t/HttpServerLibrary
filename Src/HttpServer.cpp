#include "HttpServer.h"
#include <iostream>
#include <functional>


static std::string g_strRootDir = ".";
void HttpServer::event_handle(struct mg_connection* pConn, int nEvtType, void* pEventData, void* pPara)
{
    if (MG_EV_ACCEPT == nEvtType && pPara != nullptr)
    {
        /*
            mg_tls_opts optsTls;
            std::string strServerPem = "server.pem";
            // Certificate PEM file
            memcpy((void*)optsTls.cert, strServerPem.c_str(), strServerPem.length());
            // This pem contains both cert and key
            memcpy((void*)optsTls.certkey, strServerPem.c_str(), strServerPem.length());
            mg_tls_init(pConn, &optsTls);
        */
    }
    else if (MG_EV_HTTP_MSG == nEvtType)
    {
        struct mg_http_message* pHttpMsg = (struct mg_http_message*)pEventData;
        if (mg_http_match_uri(pHttpMsg, "/api/stats"))
        {
            // Print some statistics about currently established connections
            mg_printf(pConn, "HTTP/1.1 200 OK\r\nTransfer-Encoding: chunked\r\n\r\n");
            mg_http_printf_chunk(pConn, "ID PROTO TYPE      LOCAL           REMOTE\n");
            for (struct mg_connection* t = pConn->mgr->conns; t != NULL; t = t->next) {
                char loc[40], rem[40];
                mg_http_printf_chunk(pConn, "%-3lu %4s %s %-15s %s\n", t->id,
                    t->is_udp ? "UDP" : "TCP",
                    t->is_listening ? "LISTENING"
                    : t->is_accepted ? "ACCEPTED "
                    : "CONNECTED",
                    mg_straddr(&t->loc, loc, sizeof(loc)),
                    mg_straddr(&t->rem, rem, sizeof(rem)));
            }
            mg_http_printf_chunk(pConn, "");  // Don't forget the last empty chunk
        }
        else if (mg_http_match_uri(pHttpMsg, "/api/f2/*"))
        {
            mg_http_reply(pConn, 200, "", "{\"result\": \"%.*s\"}\n", (int)pHttpMsg->uri.len,
                pHttpMsg->uri.ptr);
        }
        else
        {
            std::string strUri = std::string(pHttpMsg->uri.ptr, pHttpMsg->uri.len);
            std::cout << "--uri: " << strUri << "\n";

            std::string strBody = std::string(pHttpMsg->body.ptr, pHttpMsg->body.len);
            std::cout << "*****Body: " << strBody << "\n";
            std::string strResponse = HttpServer::Instance().do_request(strUri, strBody);
            if (strResponse.empty())
            {
                std::cout << "Response: " << strResponse << "\n";
                mg_http_reply(pConn, 200, "", "{\"result\": \"%.*s\"}\n", (int)pHttpMsg->uri.len,
                    pHttpMsg->uri.ptr);
            }
            else
            {
                mg_http_reply(pConn, 200, "", "%s\n", strResponse.c_str());
            }
            /*
                mg_http_serve_opts optsHttp;
                memcpy((void*)optsHttp.root_dir, g_strRootDir.c_str(), g_strRootDir.length());
                mg_http_serve_dir(pConn, hm, &optsHttp);
            */
        }
    }
    (void)pPara;
}

HttpServer& HttpServer::Instance()
{
    static HttpServer g_Instance;
    return g_Instance;
}

HttpServer::HttpServer()
    : m_cbResponse(nullptr)
{

}

HttpServer::~HttpServer()
{
    stop();
}

bool HttpServer::StartHttpServer(int nPort, callback_request cbResponse/* = nullptr*/, int nSslPort/* = 0*/)
{
    m_cbResponse = cbResponse;
    // Initialise event manager
    mg_mgr_init(&m_Mgr);

    // Create HTTP listener
    char szUrl[128] = { 0 };
    sprintf(szUrl, "0.0.0.0:%d", nPort);
    auto pHttpConn = mg_http_listen(&m_Mgr, szUrl, event_handle, nullptr);
    if (nullptr == pHttpConn) return false;
    if (nSslPort)
    {
        // HTTPS listener
        char szSslUrl[128] = { 0 };
        sprintf(szSslUrl, "0.0.0.0:%d", nSslPort);
        mg_http_listen(&m_Mgr, szSslUrl, event_handle, (void*)1);
    }
    m_thRun = std::thread(std::bind(&HttpServer::run, &HttpServer::Instance()));
    return true;
}

void HttpServer::StopHttpServer()
{
    stop();
}

void HttpServer::stop()
{
    m_bExit.store(true);
    if (m_thRun.joinable())
    {
        m_thRun.join();
    }
}

void HttpServer::run()
{
    while (!m_bExit.load())
    {
        mg_mgr_poll(&m_Mgr, 500); // ms
    }
    mg_mgr_free(&m_Mgr);
}

std::string HttpServer::do_request(const std::string& strUri, const std::string& strBody)
{
    char* pResponse = nullptr;
    std::string strResponse;
    if (m_cbResponse) {
        m_cbResponse(strUri, strBody, strResponse);
    }
    return strResponse;
}